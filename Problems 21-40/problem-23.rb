# Problem: Perfect numers have the sum of their divisors equal to themselves. A number is deficient if the sum
# is less than the number, and abundant if it is more than the number. Find the sum of all numbers that cannot
# be written as the sum of two abundant numbers.

def divisorSum(num) # Taken from problem 21
  sum = 0
  
  (1..Math.sqrt(num)).each do |i|
    if num % i == 0
      sum += i
      sum += num/i unless (i == 1) || (num / i == i)
    end
  end
  return sum
end

start = Time.now

# First we find all abundant numbers less than 28123 as well as get the sum of all numbers through 28123

abundant = []
all_nums = 0

(1..28123).each do |i|
  all_nums += i
end

(1..28123).each do |i|
  if divisorSum(i) > i
    abundant.push(i)
  end
end

puts "abundant[] populated"

# Now we find all numbers that can be expressed as the sum of two abundant numbers

sums = []

(0..abundant.length - 1).each do |i|
  (i..abundant.length - 1).each do |j|
    sums.push(abundant[i] + abundant[j]) unless abundant[i] + abundant[j] > 28123
  end
end

puts "sums[] populated"

# Go ahead and kill the duplicates real quick
sums = sums.uniq

# Now we just subtract the sum of all numbers which are the sum of two abundant numbers

answer = all_nums - sums.inject { |res, a| res + a }

puts answer
puts "Solved in #{Time.now - start} seconds."

# Solution: 4179871