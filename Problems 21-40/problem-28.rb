# Problem: Find the sum of the diagonals in a number spiral which is 1001 x 1001

start = Time.now

sum = 1
x = 3

while x <= 1001
  sum += (x * x * 4) - ((x - 1) * 6)
  x += 2
end

puts sum
puts "Calculated in #{Time.now - start} seconds."

# Solution: 669171001
# So proud of this one, figured it out on my own start to finish.