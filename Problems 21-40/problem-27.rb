# Find the product of a and b for the equation n**n + an + b where the values of n, starting at 0, produces
# the largest number of primes in a row

require 'prime'

start = Time.now

maxN, maxA, maxB = 0

(-1000...1000).each do |a|
  (-1000...1000).each do |b|
    primes = 0
    while ((primes*primes) + a * primes + b).prime?
      primes += 1
    end
    
    if primes > maxN
      maxN, maxA, maxB = primes, a, b
    end
  end
end

puts "The solution is #{maxA * maxB}, using #{maxA} and #{maxB}, which provide #{maxN} primes in a row."
puts "Solved in #{Time.now - start} seconds."

# Solution: -59231
# This one I got on my own, but did find some interesting things online once I'd created my answer regarding
#   ways to cut time out