# Problem: if d(n) is defined as the sum of proper divisors of n, and d(a) = b and d(b) = a, then this is
# an amicable pair of numbers. What is the sum of all amicable numbers under 10000

def divisorSum(num)
  sum = 0
  
  (1..Math.sqrt(num)).each do |i|
    if num % i == 0
      sum += i
      sum += num/i unless i == 1
    end
  end
  return sum
end

d = [0]
n = [0]

(1..10000).each do |i|
  d.push(i)
  n.push(divisorSum(i))
end

sum = 0
amicable = []

(1..10000).each do |i|
#    puts "Number: #{d[i]} Sum of Divisors: #{n[i]}."
  if d[i] == divisorSum(n[i])

#    puts "Amicable pair of #{d[i]} and #{n[i]}. Sums are #{divisorSum(d[i])} and #{divisorSum(n[i])}."
    sum += d[i] unless d[i] == n[i]
    amicable.push(d[i])
  end
  
  
end

start = Time.now
puts sum
puts "Solved in #{Time.now - start} seconds"
puts amicable

# Solution: 31626
# I had a bit of trouble at the end there because it wasn't clear to not include numbers which are
# "amicable with themselves", so t