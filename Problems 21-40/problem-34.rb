# 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
#
# Find the sum of all numbers which are equal to the sum of the factorial of their digits.
#
# Note: as 1! = 1 and 2! = 2 are not sums they are not included.

start = Time.now

def factorial(num)
  return 1 if num <= 1
  num * factorial(num - 1)
end

def digit_factorial(num)
  return 0 if num < 3
  temp = num.to_s.split("")
  temp_ints = temp.map { |a| factorial(a.to_i) }
  temp_ints.inject { |sum, a| a + sum }
end

sum = 0

(3..(factorial(9) * 7)).each { |i| sum += i if i == digit_factorial(i) }

puts sum
puts "Solved in #{Time.now - start} seconds."

# Solution: 40730
# This one was pretty easy, but I got to toy around a bit more with the array methods that I used, as well
#   as use this as an opportunity to build a recursive factorial method. I did get caught up at one point 
#   with my factorial method when a digit was zero...but it was a quick fix.