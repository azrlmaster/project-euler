# The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove 
# digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work 
# from right to left: 3797, 379, 37, and 3.
#
# Find the sum of the only eleven primes that are both truncatable from left to right and right to left.
#
# NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.

start = Time.now

require 'prime'

def truncate?(i)
  num = i.to_s.chars.to_a
  
  num.length.times do
    return false unless num.join.to_i.prime?
    num.shift
  end
  
  num2 = i.to_s.chars.to_a
  
  num2.length.times do
    return false unless num2.join.to_i.prime?
    num2.pop
  end
  
  return true
end

counter = 0
sum = 0
current_num = 11

until counter == 11
  if truncate?(current_num)
    counter += 1
    sum += current_num
  end
  current_num += 1
end

puts sum
puts "Solved in #{Time.now - start} seconds."

# Solution: 748317
# Be careful when typing answers over...My first running of the program succeeded, but I mis-typed the
#   solution on projecteuler, and ended up spending 10 minutes trying to figure out where the bug was.