# Problem: Find the sum of the numbers which are equal to the sum of the fifth powers of their digits

start = Time.now

max = 9 ** 5 * 5
sum = 0

(2...max).each do |i|
  temp = i.to_s
  if temp.split("").map{|x| x.to_i**5 }.inject{|tot, x| tot + x } == i
    sum += i
    puts i
#    puts temp.split("").map{|x| x.to_i**5 }
  end
end

puts sum
puts "Solved in #{Time.now - start} seconds."

# Solution: 443839
# I did have a bit of issue with this one because there is no given ceiling. I wrote the program just fine,
# but went to the internet to find a reasonable ceiling as I waited for it to iterate through 10^10 numbers