# Problem: Find the one millionth lexicographic permutation of 0, 1, 2, 3, 4, 5, 6, 7, 8, 9

start = Time.now

lex_perms = [0,1,2,3,4,5,6,7,8,9].permutation(10).to_a
puts lex_perms[999_999].join()

puts "Solved in #{Time.now - start} seconds."

# Solution: 2783915460
# Yet another one which sent me to the internet for help. Didn't know the permutation method existed. This is a great way to learn more about
# a language!