# We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; 
# for example, the 5-digit number, 15234, is 1 through 5 pandigital.
#
# The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and 
# product is 1 through 9 pandigital.
#
# Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 
# through 9 pandigital.
#
# HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.

start = Time.now

total = 0
sums = []

(2..99).each do |a|
  (2..9999).each do |b|
    nums = a.to_s + b.to_s + (a * b).to_s
    next unless nums.length == 9
    temp = nums.split("")
    unless temp.uniq! || sums.include?(a * b) || temp.include?('0')
      sums.push(a * b)
      total += (a * b)
      puts "#{a} * #{b} = #{a * b}"
    end
  end
end

puts total
puts "Solved in #{Time.now - start} seconds"

# Solution: 45228
# Got this one on my own! It isn't exactly efficient, but it burns through in under 2 seconds. I couldn't
#   think of a good way to figure out the limits, so I set them high and had the program print out all
#   the successful numbers. When I realized none of them would be more than 4 digits, I cut them both to
#   9999. Then, it occurred to me that there's no way both of them could be 4 digit numbers...so I cut a 
#   down to 2 digits. 
    