# The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to 
# simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.
#
# We shall consider fractions like, 30/50 = 3/5, to be trivial examples.
#
# There are exactly four non-trivial examples of this type of fraction, less than one in value, and 
# containing two digits in the numerator and denominator.
#
# If the product of these four fractions is given in its lowest common terms, find the value of the 
# denominator.

start = Time.now

def matching_char(numerator, denominator)
  match1 = false
  shared = ""
  numerator.each do |i|
    if denominator.include?(i) && i != 0
      match1 = true 
      shared = i
    end
  end
  return match1, shared
end

fractions = []

(10..99).each do |numer|
  (10..99).each do |denom|
    next if numer == denom || numer > denom
    num = numer.to_s.split("")
    den = denom.to_s.split("")
    
    match1, char = matching_char(num, den)
    
    next if char == '0'
    
    if match1
      if num[0] == char
        num.delete_at(0)
      else
        num.delete_at(1)
      end
      if den[0] == char
        den.delete_at(0)
      else
        den.delete_at(1)
      end
      
#      puts "#{numer} and #{denom}"
#      puts "#{num[0]} and #{den[0]}"
#      next if num[0] > den[0]
#        puts "numer = #{numer}, denom = #{denom}, num[0] = #{num[0]}, den[0] = #{den[0]}, char = #{char}"
    
      if numer.to_f / denom == num[0].to_f / den[0].to_i
        fractions.push([numer, denom])
#        puts "numer = #{numer}, denom = #{denom}, num[0] = #{num[0]}, den[0] = #{den[0]}, char = #{char}"
      end
    end
  end
end

top = 1
bottom = 1

fractions.each do |i|
  top *= i[0]
  bottom *= i[1]
end

if bottom % top == 0
  puts "The solution is #{bottom / top}"
end

puts "Solution took #{Time.now - start} seconds."

# Solution: 100
# There has got to be a prettier way to do this one. Definitely worth revisiting at some point to refactor
#   into something better. Also, the journey to the solution basically only works at the end for an easily
#   divisible fraction. The answer is pretty cool, though.