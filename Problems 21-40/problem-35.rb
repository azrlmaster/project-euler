# The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, 
# are themselves prime.
#
# There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
#
# How many circular primes are there below one million?

start = Time.now

require 'prime'

#def rotate_digits(arr)
#  temp = []
#  temp2 = arr
#  arr.length.times do
#    temp2.unshift(temp2[-1])
#    temp2.pop
#    puts "#{temp2[0]}, #{temp2[1]}, #{temp2[2]}"
#    temp.push(temp2)
#  end
#  puts "#{temp[0]}, #{temp[1]}, #{temp[2]}"
#  temp
#end

#def check_prime(num)
#  temp = num.to_s.chars.to_a
#  temp.each { |i| puts i }
#  rotate_digits(temp).each { |i| return false unless Prime.prime?(i.join.to_i) }
#  true
#end

def check_prime(i)
  nums = i.to_s.chars.to_a
  
  nums.length.times do 
    return false if (!nums.join.to_i.prime?)
    nums.push(nums[0])
    nums.shift
  end
  
  return true
end

count = 0

Prime.each(1_000_000) { |i| count += 1 if check_prime(i) }

puts count

#puts check_prime(197)
puts "Solved in #{Time.now - start} seconds."

# Solution: 55
# This one I wrote up and couldn't quite get it to work. I then decided to peruse the internet for ideas
#   on how to refactor the code, and quickly realized there was no reason to have 2 methods, no reason to
#   overly complicate things by using so many variables and silliness.