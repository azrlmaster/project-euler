# Problem: For a**b, and 2 <= a <= 100 and 2 <= b <= 100, how many unique terms are generated?

start = Time.now

nums = []

(2..100).each do |a|
  (2..100).each do |b|
    nums.push(a**b)
  end
end

puts nums.uniq!.length
puts "Solved in #{Time.now - start} seconds."

# Solution: 9183
# This one was pretty dang easy, mainly because of the array.uniq! method built into Ruby