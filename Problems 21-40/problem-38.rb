# Take the number 192 and multiply it by each of 1, 2, and 3:
#
# 192 × 1 = 192
# 192 × 2 = 384
# 192 × 3 = 576
#
# By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the 
# concatenated product of 192 and (1,2,3)
#
# The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 
# 918273645, which is the concatenated product of 9 and (1,2,3,4,5).
#
# What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of 
# an integer with (1,2, ... , n) where n > 1?

start = Time.now

def conc(num)
  conc = ""
  mult = 1
  
  until conc.length >= 9
    conc += (num * mult).to_s
    mult += 1
  end
  
  if conc.length == 9 && !(conc.chars.to_a.include?('0')) && conc.chars.to_a.uniq.length == 9
    return conc.to_i
  else
    return false
  end
end

high = 0

(1..10000).each do |i|
  sum = conc(i)
  if sum && sum > high
    high = sum
  end
end

puts high
puts "Solved in #{Time.now - start} seconds."

# Solution: 932718654
# This one was another fairly easy problem, although I did have to toy around with calling the correct
#   methods. I didn't realize that Array.uniq! would return nil if it doesn't remove anything, but 
#   Array.uniq returns the array.