# Problem: How many different combinations of coins can be used to produce 2 pounds? 
# Coins in circulation: 1p, 2p, 5p, 10p, 20p, 50p, 1 pound, 2 pounds

start = Time.now

def coin_combinations(types, total, coin_index)
  return 1 if coin_index == 0
  return 0 if total < 0
  coin_combinations(types, total - types[coin_index], coin_index) + coin_combinations(types, total, coin_index - 1)
end

puts coin_combinations([1, 2, 5, 10, 20, 50, 100, 200], 200, 7)
puts "Solved in #{Time.now - start} seconds"

# Solution: 73682
# This one threw me for a loop at first. I get the idea of recursive formulas and built a couple test cases
#   to enhance my knowledge of them...but this one just didn't work for me. I ended up finding a bunch of
#   solutions that I still didn't understand, so I had to delve into Wikipedia and find the number theory
#   involved in this problem.