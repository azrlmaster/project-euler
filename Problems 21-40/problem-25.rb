# What is the first Fibonacci number with 1000 digits?

start = Time.now

def fibonacci(a, b)
  if (a && b) == 1
    return 1, 2
  else
    return b, a + b
  end
end

f1 = 1
f2 = 1
index = 2

until f2.to_s.length >= 1000
  f1, f2 = fibonacci(f1, f2)
  index += 1
end

puts index
puts "Solved in #{Time.now - start} seconds."

# Solution: 4782