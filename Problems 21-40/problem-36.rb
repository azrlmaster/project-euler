# The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.
#
#Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
#
#(Please note that the palindromic number, in either base, may not include leading zeros.)

start = Time.now

def palindrome?(str)
  str == str.reverse
end

sum = 0

1_000_000.times do |i|
  if palindrome?(i.to_s)
    if palindrome?(i.to_s(2))
      sum += i
    end
  end
end

puts sum
puts "Solved in #{Time.now - start} seconds."

# Solution: 872187
# This one, I imagine, is mainly easy because of the core classes in Ruby. Literally a 5-minute problem.