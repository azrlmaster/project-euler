# Problem: From 1/2 to 1/1000, what is the value of 1/n that yields the largest recurring cycle of digits?

start = Time.now

require 'prime'

def cycle(n)
  return 0 if n % 2 == 0 || n % 5 == 0
  
  i = 1
  until (((10**i) - 1) % n) == 0
    i += 1
  end
  
  i
end

num = 0
length = 0

Prime.each(1000).each do |i|
  temp = cycle(i)
  if temp > length
    num, length = i, temp
  end
end

puts num
puts length
puts "Solved in #{Time.now - start} seconds."

# Solution: 983
# This one got a bit into math theory that I did a poor job of researching. So instead I found a solution
#   and stole the sources for his research. Interesting stuff, and pretty specific.