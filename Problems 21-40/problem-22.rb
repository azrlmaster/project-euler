# Problem: Using the given text file, first alphabetize the list. Then find the number value of each name,
# and multiply it by the place in the list. Provide the sum.

temp = IO.read("../resources/problem-22")

temp.gsub!("\"", "")
names = temp.split(",").sort

values = [0]
("a".."z").each do |i|
  values.push(i)
end

sum = 0

names.each do |name|
  temp = 0
  name.length.times do |chars|
    temp += values.index(name[chars].downcase)
  end
  sum += temp * (names.index(name) + 1)
end

puts sum

# Solution: 871198282