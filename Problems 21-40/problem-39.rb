# If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly 
# three solutions for p = 120.
#
# {20,48,52}, {24,45,51}, {30,40,50}
#
# For which value of p ≤ 1000, is the number of solutions maximised?

start = Time.now

def pyth?(a, b, c)
  (a * a) + (b * b) == c * c
end

def solutions(per)
  count = 0

  (1..(per / 2)).each do |a|
    (1..(per / 2)).each do |b|
      next if b > (per - a - b) || a > (per - a - b)
      count += 1 if pyth?(a, b, per - a - b)
    end
  end
  return count
end

max = 0
answer = 0

(3..1000).each do |i|
  count = solutions(i)
  if count > max
    max = count
    answer = i
  end
end

puts answer
puts "Computed in #{Time.now - start} seconds"

# Solution: 840
# Below you can see the remnants of what I was trying to write at first...I took a few days off, came back
#   and wrote the above code in one go and got it right :D



#def pythagorean_triples(num)
#  
#  triples = []
#  
#  (1..((num / 2) - 1)).each do |a|
#    b = num * ( (num - (a * 2)) / (2 * (num - a)) )
#    c = Math.sqrt((a * a) + (b * b))
#    if (a * a) + (b * b) == c * c
#      triples.push([a, b, c])
#    end
#  end
#  return triples.length
#end
  
#max = 0
#solution = 0
#(4..1000).each do |i|
#  triples = pythagorean_triples(i)
#  solution = i if triples > max
#  max = triples if triples > max
#end

#puts solution
  
  
  
#  a + b + c = num
#  a + b > c
#  c > a
#  c > b
#  num = a + b + Math.sqrt( (a * a) + (b * b) )
  