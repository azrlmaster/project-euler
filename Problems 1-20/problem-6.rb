# Problem: Find the difference between the sum of the squares and the square of the sums of 1 - 100

sum_of_squares = 0
square_of_sums = 0

(1..100).each do |i|
  sum_of_squares += (i * i)
end

(1..100).each do |i|
  square_of_sums += i
end

square_of_sums = square_of_sums * square_of_sums

puts square_of_sums - sum_of_squares

# Solution: 25164150