# Problem: What is the sum of the digits of 2^1000?

num = 2

999.times {num *= 2}

nums = num.to_s

total = 0
nums.length.times {|i| total += nums[i].to_i}

puts total

# Solution: 1366