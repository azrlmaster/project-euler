# Problem: How many characters are used to spell out every number from 1 to 1000?



def count(num)
  ones  = %w[one two three four five six seven eight nine]
  teens = %w[eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen]
  tens  = %w[ten twenty thirty forty fifty sixty seventy eighty ninety]

  words = []
  
  while num > 0
    if num / 1000 == 1
      words << "one thousand"
      num -= 1000
    elsif num / 100 >= 1
      words << "#{ones[(num / 100) - 1]} hundred"
      num -= (num / 100) * 100
      if num > 0
        words << "and"
      end
    elsif num / 10 > 0 
      if num / 10 == 1 && num != 10
        words << "#{teens[(num % 10) - 1]}"
        num = 0
      else
        words << "#{tens[(num / 10) - 1]}"
        num -= (num / 10) * 10
      end
    else
      words << "#{ones[num - 1]}"
      num = 0
    end
  end
  
  words.join(" ")
end

start = Time.now
characters = 0

(1..1000).each do |i|
#  puts count(i)  # Just for testing, originally I had an issue where it skipped all of the hundreds :/
  characters += count(i).gsub(" ", "").length
end

puts characters
puts "Solved in #{Time.now - start} seconds"
  
# Solution: 21124