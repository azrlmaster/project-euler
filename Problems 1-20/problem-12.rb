# Problem: What is the value of the first triangle number with over 500 divisors?

num = 0
counter = 1

divisors = []

while divisors.length <= 500
  divisors = []
  num += counter
  counter += 1
  
  (1..Math.sqrt(num)).each do |i|
    if num % i == 0
      divisors.push(i)
      divisors.push(num/i)
    end
  end
end

puts num

# Solution: 76576500