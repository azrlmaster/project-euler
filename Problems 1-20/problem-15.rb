# Problem: How many routes are there through a 20x20 grid, only moving right and down?

def route(size)
  size = size + 1
  
  path = []
  
  size.times do |i|
    path.push([])
  end
  
  size.times do |i|
    path[i].push(1)
  end
  
  (1...size).each do |j|
    path[0].push(1)
  end
  
  (1...size).each do |i|
    (1...size).each do |j|
      path[i].push(path[i][j-1] + path[i-1][j])
    end
  end

  (0...size).each do |j|
    (0...size).each do |i|
      print "#{path[i][j]} "
    end
    puts ""
  end
  
  size -= 1
  
  puts "The total number of routes for a square of #{size} sides is #{path[size][size]}."
  
end

route(20)

# Solution: 137846528820
# I'll admit, this one stumped me for a while. I looked up some online resources and then mapped it out
#   myself before getting it right.