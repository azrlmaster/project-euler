# Problem: What is the product of the only pythagorean triple whose sum is 1000

def pyth?(a, b, c)
  if (a * a) + (b * b) == (c * c)
    true
  else
    false
  end
end

def reset(a, b, c)
  until b >= c
    b += 1
    c = 1000 - a - b
  end
  
  b -= 1
  c = 1000 - a - b
  
  return b, c
end

a = 332
b = 333
c = 335

until pyth?(a, b, c)
  while a < b
    break if pyth?(a, b, c)
    b -= 1
    c += 1
  end
  break if pyth?(a, b, c)
  a -= 1
  b, c = reset(a, b, c)
end

puts a, b, c
puts a * b * c
    
# Solution: 31875000