# Problem: What is the path with the highest sum of the given triangle?

def pull_data
  
  input = IO.read("./resources/problem-18")
  setup = input.each_line.map{|line| line.split.map(&:to_i)}

  return setup
end

temp = pull_data

puts temp.inject([]){|res,x|
  maxes = [0, *res, 0].each_cons(2).map(&:max)
  x.zip(maxes).map{|a,b| a+b}
}.max

# Solution: 1074
# This was not an answer I came up with on my own. I found it online, and spent ~2 hours trying to figure 
#   out how it works. I think I have an idea now, but honestly I need tow ork with inject, zip, and map
#   more to really get it. Basically it creates maxes each iteration by adding a leading and trialing zero
#   for the starting line. Then it finds the max of each pair, going through each possible pair on the array.
#   It then zips the maxes into the next line and adds the new pairs together to get each possible choice as
#   you move down the triangle. It then uses the new array for the next iteration.