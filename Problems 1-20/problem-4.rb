# Problem: Find the highest number that is also a palindrome which results from multiplying 2 3-digit numbers

def palindrome?(num)
  
  temp = num.to_s
  array = temp.split("")
  array2 = []
  array.each {|i| array2.unshift(i)}
  
  if array == array2
    true
  else
    false
  end
end


first_counter = []
(100..999).each {|i| first_counter.unshift(i)}
second_counter = first_counter.clone
answer = 0

first_counter.each do |i|
  second_counter.each do |j|
    if palindrome?(i * j)
      if i * j > answer
        answer = i * j
      end
    end
  end
end

puts "The largest palindrome from two 3-digit numbers is #{answer}!"

# The answer is 906609