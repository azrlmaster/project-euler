# Problem: Find the first 10 digits of the sum of the given 100 numbers

nums = IO.readlines("./resources/problem-13")

nums.each {|i| i.gsub('\n', '')}

total = 0

nums.each {|i| total += i.to_i }

total_s = total.to_s

(0..9).each {|i| print total_s[i] }

puts ""

# Solution: 5537376230