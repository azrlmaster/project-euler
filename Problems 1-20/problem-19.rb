# Problem: How many Sundays during the 20th century occurred on the first of the month?
# 1 Jan 1900 was a Monday

# days = %w[Sunday Monday Tuesday Wednesday Thursday Friday Saturday]
months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

year = 1901
month = 0
day_of_week = 3
count = 0

while year < 2001

  if year % 4 == 0 && month == 1
    day_of_week += (months[month] + 1) % 7
  else
    day_of_week += months[month] % 7
  end
  day_of_week = day_of_week % 7
  if day_of_week == 0
    count += 1
  end

  month += 1
  if month > 11
    year += 1
    month = 0
  end
end

puts count

# Solution: 171