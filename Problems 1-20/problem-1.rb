multiples = []

(1...1000).each do |i|
    if (i % 3) == 0 || (i % 5) == 0
        multiples.push(i)
    end
end

total = 0

multiples.each do |i|
    total += i
end

puts total

## Solution: 233168