# Problem: What is the 10001st prime number?

require 'prime'

puts Prime.first(10001)

# Solution: 104743
# Not exactly an elegant solution, but it worked