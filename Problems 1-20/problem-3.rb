# Determine the largest prime factor of 600851475143
require 'prime'

num = 600851475143
factors = []

(3..Math.sqrt(num)).each do |i|
  if i % 2 == 0
    next
  end
  
  while num % i == 0
    factors.push(i)
    num = num / i
  end
end

puts factors

# Solution is 6857