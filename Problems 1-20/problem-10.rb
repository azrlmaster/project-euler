# Problem: What is the sum of all prime numbers below 2 million?

require 'prime'

sum = 0

Prime.each(2000000) {|i| sum += i}

puts sum

# Solution: 142913828922
# Starting to feel bad about using the Prime class built into Ruby, but hey