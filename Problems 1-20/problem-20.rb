# Problem: Find the sum of the digits of 100!

sum = (1..100).inject {|a, b| a * b }

array = sum.to_s
array2 = array.split('')

puts array2.map(&:to_i).inject {|a, b| a + b }

# Solution: 648
# See, this is what I mean about geting more practice with inject and map :)