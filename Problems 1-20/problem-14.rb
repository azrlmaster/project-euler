# Problem: What starting number under 1 million produces the longest Collatz sequence?

def collatz(num)
  values = []
  
  while num > 1
  
    if num % 2 == 0
      num = num / 2
    else
      num = (3 * num) + 1
    end
    
    values.push(num)
  end
  
  values
end

answer = 0
size = 0
(1..1000000).each do |i|
  temp = collatz(1000000 - i)
  if temp.length > size
    size = temp.length
    answer = 1000000 - i
  end
end

puts answer

# Solution: 837799