# Problem: Find the smallest number which is evenly divisible by every number 1-20
# Every 
done = false
counter = 20

until done
  done = true
  (11..19).each do |i|
    if counter % i != 0
      done = false
#      puts counter % i
      break
    end
#    puts i
  end
  counter += 20
#  done = true
end

counter -= 20

puts counter

# Solution: 232792560
# I had the issue where the algorithm added 20 to my counter even after it found the correct solution :(