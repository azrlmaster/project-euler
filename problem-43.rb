# The number, 1406357289, is a 0 to 9 pandigital number because it is made up of each of the digits 0 to 9 in some order, 
# but it also has a rather interesting sub-string divisibility property.
#
# Let d1 be the 1st digit, d2 be the 2nd digit, and so on. In this way, we note the following:
#
# d2d3d4=406 is divisible by 2
# d3d4d5=063 is divisible by 3
# d4d5d6=635 is divisible by 5
# d5d6d7=357 is divisible by 7
# d6d7d8=572 is divisible by 11
# d7d8d9=728 is divisible by 13
# d8d9d10=289 is divisible by 17
# Find the sum of all 0 to 9 pandigital numbers with this property.

start = Time.now

def nextPan(array)
  i = array.length - 1
  
  while i > 0 && array[i-1] >= array[i]
    i -= 1
  end
    
  return array if i <= 0
  
  j = array.length - 1
  
  while array[j] <= array[i-1]
    j -= 1
  end
  
  temp = array[i-1]
  array[i-1] = array[j]
  array[j] = temp
  
  j = array.length - 1
  while i < j
    temp = array[i]
    array[i] = array[j]
    array[j] = temp
    i += 1
    j -= 1
  end
  
  return array
end

def join(array, start, num)
  i = 0
  final = ""
  while i < num
    final += array[start - 1 + i].to_s
    i += 1
  end
  
  return final.to_i
end
    

def checkDiv(array)
  return false if join(array, 2, 3) % 2 != 0
  return false if join(array, 3, 3) % 3 != 0
  return false if join(array, 4, 3) % 5 != 0
  return false if join(array, 5, 3) % 7 != 0
  return false if join(array, 6, 3) % 11 != 0
  return false if join(array, 7, 3) % 13 != 0
  return false if join(array, 8, 3) % 17 != 0
  true
end

def compare(arr1, arr2)
  arr1.length.times do |i|
    return false unless arr1[i] == arr2[i]
  end
  true
end

array = [0,1,2,3,4,5,6,7,8,9]
sum = 0
continue = true

while continue
  temp = array.clone
  array = nextPan(array)
  continue = false if compare(array, temp)
#  puts array
  sum += join(array, 1, 10) if checkDiv(array)
end

puts sum
puts "Solved in #{Time.now - start} seconds"

# Solution: 16695334890
# I really enjoyed writing this one as making the various methods I used was pretty fun. I did steal the permutation algorithm
#   and converted it a bit to Ruby, but that's programming, right?