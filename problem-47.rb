# The first two consecutive numbers to have two distinct prime factors are:
#
# 14 = 2 × 7
# 15 = 3 × 5
#
# The first three consecutive numbers to have three distinct prime factors are:
#
# 644 = 2² × 7 × 23
# 645 = 3 × 5 × 43
# 646 = 2 × 17 × 19.
#
# Find the first four consecutive integers to have four distinct prime factors. What is the first of these numbers?

start = Time.now

require 'prime'

def factor(num)
  
  factors = []
  counter = 2
  
  if Prime.prime?(num)
    factors.push(num)
    return factors
  end
  
  until Prime.prime?(num)
    unless Prime.prime?(counter)
      counter += 1
      next
    end
    if num % counter == 0
      factors.push(counter)
      num = num / counter
      counter = 2
      # puts counter
      # puts num
    else
      counter += 1
    end
  end
  factors.push(num)
end

def uniqueFactors?(array, number)
  return false if array.length == 1
  if array.group_by{|x| x}.map{|k,v| [k,v.count] }.length >= number
    true
  else
    false
  end
end

count = 2
nums = []
total_factors = 4

until nums.length == total_factors
  # puts count
  if Prime.prime?(count)
    nums = []
    count += 1
    next
  elsif uniqueFactors?(factor(count), total_factors)
    nums.push(count)
  else
    nums = []
  end
  count += 1
end

puts nums
puts "Solved in #{Time.now - start} seconds."

# Solution: 134043
# I mean, it definitely isn't an elegant solution, but it does get the job done. Starting at 2 (which is a bit of a waste
#   with 210 being lowest number with 4 factors, but it can be adapted for different factor counts) it takes just over
#   100 seconds to run...but it does run!