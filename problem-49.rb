# The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: 
# (i) each of the three terms are prime, and, 
# (ii) each of the 4-digit numbers are permutations of one another.

# There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, 
# but there is one other 4-digit increasing sequence.

# What 12-digit number do you form by concatenating the three terms in this sequence?

require 'prime'

start = Time.now

def threePerms?(i, j, k)
  i, j, k = i.to_s.chars.sort.join, j.to_s.chars.sort.join, k.to_s.chars.sort.join
  return true if i == j && i == k
  false
end
  
solution = ""
floor = 1000
ceiling = 10000

Prime.each do |i|
  next if i < floor
  break if i >= ceiling
  
  Prime.each do |j|
    next if j < floor
    break if j >= ceiling
    next if j <= i
    
    k = j + (j - i)
    
    next if k >= ceiling || !Prime.prime?(k)
    
    solution = "#{i}#{j}#{k}" if threePerms?(i, j, k)
    
    break unless solution.empty? || solution == "148748178147"
  end
  break unless solution.empty? || solution == "148748178147"
end

puts solution
puts "Solved in #{Time.now - start} seconds."

# Solution: 296962999629
# I had some difficulty understanding exactly what this question was asking at first, but once I found a better
#   explanation online, I was able to work this out. It took a minute to establish the relationships between the numbers
#   (k = j + (j - i), etc), but then the rest was easy. It even ran in under 1 second.