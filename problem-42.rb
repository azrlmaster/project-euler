# The nth term of the sequence of triangle numbers is given by, tn = ½n(n+1); so the first ten triangle 
# numbers are:
# 
# 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
#
# By converting each letter in a word to a number corresponding to its alphabetical position and adding 
# these values we form a word value. For example, the word value for SKY is 19 + 11 + 25 = 55 = t10. If the 
# word value is a triangle number then we shall call the word a triangle word.
#
# Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing nearly 
# two-thousand common English words, how many are triangle words?

start = Time.now

@letter_values = {"A" => 1, "B" => 2, 'C' => 3, 'D' => 4, 'E' => 5, 'F' => 6, 
                 'G' => 7, 'H' => 8, 'I' => 9, 'J' => 10, 'K' => 11, 'L' => 12,
                 'M' => 13, 'N' => 14, 'O' => 15, 'P' => 16, 'Q' => 17, 'R' => 18,
                 'S' => 19, 'T' => 20, 'U' => 21, 'V' => 22, 'W' => 23, 'X' => 24,
                 'Y' => 25, 'Z' => 26 }

def word_value(str)
  val = 0
  (0..str.length - 1).each {|i| val += @letter_values[str[i]]}
  return val
end

words = IO.read('resources/problem-42').gsub('"', '').split(',')
triNums = []
(1..100).each {|i| triNums.push((i**2 + i) / 2)}

count = 0

words.each {|word| count += 1 if triNums.include?(word_value(word)) }

puts count
puts "Computed in #{Time.now - start} seconds."

# Solution: 162
# This one wasn't exactly that complicated once I wrote it down and looked at it for a second. I was 
#   reaching for a second, trying to do the math without the hash of letter values, but what's the point?