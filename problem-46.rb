# It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and 
# twice a square.
#
# 9  = 7  + 2×1^2
# 15 = 7  + 2×2^2
# 21 = 3  + 2×3^2
# 25 = 7  + 2×3^2
# 27 = 19 + 2×2^2
# 33 = 31 + 2×1^2
#
# It turns out that the conjecture was false.
#
# What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?

require 'prime'

start = Time.now

def twiceSquare(n)
  temp = Math.sqrt(n / 2)
  temp == temp.to_i
end

counter = 1
found = false

until found
  counter += 2
  next if Prime.prime?(counter)
  found = true
  Prime.each(counter) do |i|
    if twiceSquare(counter - i)
      found = false
      break
    end
  end
end

puts counter
puts "Solved in #{Time.now - start} seconds."

# Solution: 5777
# I originally wrote a cumbersome brute-force approach that performed WAY too many calculations, so I browsed the internet
#   for an explanation of what Goldblach's Conjecture was all about. I eventually ended up deciding to check against the
#   double of a square instead of the counter itself after coming across a discussion of this problem.