# We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. 
# For example, 2143 is a 4-digit pandigital and is also prime.
#
# What is the largest n-digit pandigital prime that exists?

require 'prime'

start = Time.now

def pandigital?(num)
  arr = num.to_s.split("")
  (1..arr.length).each do |i|
    return false unless arr.include?(i.to_s)
  end
  true
end

answer = 0

Prime.each(10000000) do |i|
  answer = i if pandigital?(i)
end

puts answer
puts "Solved in #{Time.now - start} seconds."

# Solution: 7652413
# This one was another fairly easy one to build out once I got my head wrapped around the concept. 