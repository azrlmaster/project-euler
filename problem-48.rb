# The series, 1**1 + 2**2 + 3**3 + ... + 10**10 = 10405071317.

# Find the last ten digits of the series, 1**1 + 2**2 + 3**3 + ... + 1000**1000.

start = Time.now

total = 0

(1...1000).each do |i|
  total += i**i
end

total = total.to_s

puts total.slice(-10, 10)
puts "Solved in #{Time.now - start} seconds."

# Solution: 9110846700
# This one was quite easy, as you can see. Maybe Ruby is just good for this kind of problem.