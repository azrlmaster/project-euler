# Project Euler Problems
## Bryan Marvin's solutions

This is a repository to store and *ahem* show off a little bit.

Please feel free to visit [Project Euler](projecteuler.net) to get a slightly more in-depth explanation on any of the problems.